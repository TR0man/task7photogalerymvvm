package com.example.task7galerywithmvvm.utils

interface AdapterClickListener {
    fun onElementClick(position: Int)
    fun onElementLongClick(position: Int)
}