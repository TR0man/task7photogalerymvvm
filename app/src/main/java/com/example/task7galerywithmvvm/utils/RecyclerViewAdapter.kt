package com.example.task7galerywithmvvm.utils

import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.task7galerywithmvvm.R
import com.example.task7galerywithmvvm.databinding.ListElementBinding

class RecyclerViewAdapter (
    private val elements: MutableList<Uri>,
    private val elementClickListener: AdapterClickListener) : RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder>() {

    private var lastSelectedItem: RecyclerViewHolder? = null

    inner class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        // "подключаем" ViewBinding к нашему элементу RecycledView (в данном случае list_element)
        private val binding = ListElementBinding.bind(itemView)
        var image: ImageView? = null

        init {
            image = binding.ivMiniPhoto
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val elementView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_element, parent, false)
        return RecyclerViewHolder(elementView)
    }


    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.image?.setImageURI(elements[position])


        holder.image?.setOnClickListener {
            elementClickListener.onElementClick(position)

            // можно убрать (только для декора) поставить margin="4dp" убрать android:padding="2dp"
            lastSelectedItem?.let {
                it.image?.setBackgroundColor(Color.argb(255, 255, 255, 255))
            }
            holder.image?.setBackgroundColor(Color.argb(255, 0, 0, 170))
            lastSelectedItem = holder
        }

        holder.image?.setOnLongClickListener{
            elementClickListener.onElementLongClick(position)
            return@setOnLongClickListener true
        }
    }

    override fun getItemCount(): Int {
        return elements.size
    }

    // добавляем новй элемент в список + уведомляем о том, что список элементов изменился
    fun addNewElement(element: Uri) {
        elements.add(element)
        notifyDataSetChanged()
    }

    fun removeElement(position: Int) {
        elements.removeAt(position)
        notifyDataSetChanged()
    }

    fun updateStatus(position: Int) {
        notifyItemChanged(position)
    }
}