package com.example.task7galerywithmvvm.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import kotlin.math.pow

class CustomPhotoButton(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    private var paint = Paint()
    private var canvasCenterX = 0f
    private var canvasCenterY = 0f
    private var circleRadius = Constants.DEFAULT_CUSTOM_CIRCLE_RADIUS
    private var rectSize = circleRadius * 4
    private var rectCornerRadius = Constants.DEFAULT_CUSTOM_CORNER_RADIUS

    init {
        getAttributeData(attrs)
    }

    private fun getAttributeData(parameter: AttributeSet?) {
        paint.apply {
            color = Constants.DEFAULT_CUSTOM_BUTTON_COLOR
            paint.strokeWidth = Constants.DEFAULT_CUSTOM_STROKE_WIDTH
            paint.style = Paint.Style.STROKE
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension((rectSize + Constants.DEFAULT_CUSTOM_STROKE_WIDTH).toInt(),
            (rectSize + Constants.DEFAULT_CUSTOM_STROKE_WIDTH).toInt())
    }

    override fun onDraw(canvas: Canvas?) {
        canvasCenterX = width / 2f
        canvasCenterY = height / 2f

        canvas?.drawCircle(canvasCenterX, canvasCenterY, circleRadius, paint)

        canvas?.drawRoundRect(
            canvasCenterX - rectSize / 2f,
            canvasCenterY - rectSize / 2f,
            canvasCenterX + rectSize / 2f,
            canvasCenterY + rectSize / 2f,
            rectCornerRadius, rectCornerRadius, paint)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_UP -> {
                // вычисляем расстояние между 2 точками (по теореме Пифагора)
                val distance = kotlin.math.sqrt(
                    (canvasCenterX - event.x).pow(2)
                            + (canvasCenterX - event.y).pow(2)
                )
                // если попали в если попали в область то действуем
                if (distance <= (rectSize + 17) / 2) {
                    performClick()
                }
            }
        }
        return true
    }

}