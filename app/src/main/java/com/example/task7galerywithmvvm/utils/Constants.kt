package com.example.task7galerywithmvvm.utils

import android.Manifest
import android.graphics.Color

class Constants {

    companion object {
        const val PREFERENCES_KEY = "path"
        const val DIRECTORY_WITH_PHOTO_NAME = "PhotoAlbum"
        const val ACTIVITY_REQUEST_CODE = 100
        const val PERMISSION_REQUEST_CODE = 200
        val PERMISSION_LIST = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE)

        val DEFAULT_CUSTOM_BUTTON_COLOR = Color.argb(150,230,80,0)
        const val DEFAULT_CUSTOM_STROKE_WIDTH = 17F
        const val DEFAULT_CUSTOM_CIRCLE_RADIUS = 35F
        const val DEFAULT_CUSTOM_CORNER_RADIUS = 35F

    }
}