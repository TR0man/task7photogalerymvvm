package com.example.task7galerywithmvvm.viewmodel

import android.app.Application
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.view.View
import android.view.animation.AnimationUtils
import androidx.core.content.FileProvider
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.task7galerywithmvvm.BuildConfig
import com.example.task7galerywithmvvm.R
import com.example.task7galerywithmvvm.utils.AdapterClickListener
import com.example.task7galerywithmvvm.utils.Constants
import com.example.task7galerywithmvvm.utils.RecyclerViewAdapter
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


// не должно быть Андроид зависимостей (view, activity, context) прямых сетевых или SQL запросов     (не выходит)

// основная логика работы приложения
// отправка запросов на создание и удаление файлов
// получение данных от Model и неявная передача данных во View (на экран) через LiveData / DataBinding

class GalleryViewModel (application: Application) : AndroidViewModel(application), AdapterClickListener {


    var photoDirectoryPath: File? = null
    private var photoList = mutableListOf<Uri>()
    var directoryUri: Uri? = null

    var lastPhoto = MutableLiveData<Uri>()
    var pathForSaveInPreferences = MutableLiveData<File>()

    var dialogViewer = MutableLiveData<Int>()

    var recyclerViewState = MutableLiveData<Boolean>()

    // получаем путь к папке с фото и сохраняем в переменной
    fun loadPhotoDirectoryPath(path: String) {
        photoDirectoryPath = File(path)
    }

    // получение списка сохраненных файлов в папке с моими фото                                     ( !!! нужен Context )
    fun getSavedPhoto() {
        photoDirectoryPath?.let {
            val files = it.absoluteFile.listFiles()
            files.sortWith(kotlin.Comparator { o1, o2 -> o1.lastModified().compareTo(o2.lastModified()) })      // сортируем по времени создания файлов

            files?.forEach {
                photoList.add(
                    FileProvider.getUriForFile(
                        getApplication(), BuildConfig.APPLICATION_ID + ".provider", it))
            }
            if (photoList.isNotEmpty()) {
                lastPhoto.value = photoList.last()
            }
        }
    }

    // создание папки под фото + сохранеие пути к папке в Preferences                               ( !!! нужен Context )
    fun directoryForPhoto(): Uri? {
        directoryUri = getAlbumStorageDirectory(getApplication(), Constants.DIRECTORY_WITH_PHOTO_NAME)
        return directoryUri
    }

    // создание папки под фото + сподготовка данных для сохранения в Preferences
    private fun getAlbumStorageDirectory(context: Context, albumName: String): Uri {
        if (photoDirectoryPath == null) {
            photoDirectoryPath = File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), albumName)
            photoDirectoryPath?.let {
                if (!it.exists()) {
                    it.mkdirs()
                }
            }
            pathForSaveInPreferences.value = photoDirectoryPath
        }

        val file = File(photoDirectoryPath?.path + File.separator + "photo_" +
                SimpleDateFormat("ddMMy_HHmmss").format(Date()) + ".jpg")

        return FileProvider.getUriForFile(getApplication(), BuildConfig.APPLICATION_ID + ".provider", file)
    }

    // возвращаем список всех картинок в нашей папке с картинками
    fun getPhotoList(): MutableList<Uri> {
        return photoList
    }

    // добавляем новое фото и обновляем RecyclerView после добавления
    fun addNew(newPhoto: Uri?, adapterRW: RecyclerViewAdapter?) {
        newPhoto?.let {
            adapterRW?.addNewElement(newPhoto)
        }
    }
    // удаление из списка фото в заданной позиции
    fun deletePhoto(position: Int, adapterRW: RecyclerViewAdapter?) {
        adapterRW?.removeElement(position)
    }

    fun updateStatus(position: Int, adapterRW: RecyclerViewAdapter?) {
        adapterRW?.updateStatus(position)
    }

    // делаем "главной" выбранную мини картинку
    override fun onElementClick(position: Int) {
        lastPhoto.value = photoList[position]
    }

    // действия по продолжительному клику по мини-картинке (удаление файла, обновление данных)
    override fun onElementLongClick(position: Int) {
        dialogViewer.value = position
    }

    // получение направления свайпа и обработка результата (появление или исчезание списка картинок)
    fun swapDirectionAction(startY: Int, endY: Int, miniImageListState: RecyclerView) {
        if (startY > endY) {
            if (miniImageListState.visibility == View.INVISIBLE) {
                return
            }
            miniImageListState.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.animation_hide))
            recyclerViewState.value = false
        } else {
            if (miniImageListState.visibility == View.VISIBLE) {
                return
            }
            miniImageListState.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.animation_show))
            recyclerViewState.value = true

        }
    }

}

