package com.example.task7galerywithmvvm

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.task7galerywithmvvm.databinding.ActivityMainBinding
import com.example.task7galerywithmvvm.utils.Constants
import com.example.task7galerywithmvvm.utils.RecyclerViewAdapter
import com.example.task7galerywithmvvm.viewmodel.GalleryViewModel
import java.io.File

// 1. получение и проверка разрешений
//      камера
//      разрешения на использование карты памяти

// 2. отображение фото или заглушки на экране
// (возможно это во ViewModel) контроль за касаниями и определение свапа


class MainActivity : AppCompatActivity(), View.OnTouchListener{

    lateinit var binding: ActivityMainBinding
    lateinit var viewModel: GalleryViewModel

    var adapterRW: RecyclerViewAdapter? = null

    private var startPositionY = 0
    private var endPositionY = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.mainLayout.also { it.setOnTouchListener(this) }     // подвешиваем слушателя на основной layout (Constraint Layout)

        viewModel = ViewModelProvider(this).get(GalleryViewModel::class.java)

        loadPreferences()
        getSavedPhoto()

        // отслеживаем последнее фото в списке, что бы сделать его основным фото
        viewModel.lastPhoto.observe(this, androidx.lifecycle.Observer {
            binding.ivMainPhoto.setImageURI(it)
        })
        // отслеживаем путь к папке с фото, для сохранения в Preferences
        viewModel.pathForSaveInPreferences.observe(this, androidx.lifecycle.Observer {
            savePreferences(it)
        })
        // отслеживаем долгое нажатие на фото, что бы запусть диалог удаления
        viewModel.dialogViewer.observe(this, Observer {
            createPhotoDeleteDialog(it)
        })
        // отслеживаем состояние видимости Recycler View, показываем / прячем при необходимости
        viewModel.recyclerViewState.observe(this, Observer {
            if (!it) {
            binding.rwMiniImageList.visibility = View.INVISIBLE
            } else {
            binding.rwMiniImageList.visibility = View.VISIBLE
            }
        })

        adapterRW = RecyclerViewAdapter(viewModel.getPhotoList(), viewModel)
        binding.rwMiniImageList.layoutManager = LinearLayoutManager(
            this, LinearLayoutManager.HORIZONTAL, false)
        binding.rwMiniImageList.adapter = adapterRW

        addItemTouchHelper()
    }

    // подвязка слушаетеля свапов для RecyclerView                                                  (т.к. создание RecyclerView, то и подвязка ItemTouchHelper тоже тут)
    // отслеживаем свайп по элементам RecyclerView и запускаем процедуру удаления выбранного файла
    private fun addItemTouchHelper() {
        // подвязка слушаетеля свапов для RecyclerView
        val swapHelper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.UP) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                viewModel.onElementLongClick(viewHolder.layoutPosition)
            }
        })
        swapHelper.attachToRecyclerView(binding.rwMiniImageList)
    }

    // отслеживание направления свайпа и передача данных во ViewModel для обработки                 (знаю как повесить/получить данные от слушателя только тут, из ViewModel не выходит)
    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                startPositionY = event.y.toInt()
            }
            MotionEvent.ACTION_UP -> {
                endPositionY = event.y.toInt()
                viewModel.swapDirectionAction(startPositionY, endPositionY, binding.rwMiniImageList)
            }
        }
        return true
    }

    // проверка необходимых разрешений для работы приложения и сохранения данных                    (не знаю как получить/провекрить разрешения из ViewModel)
    private fun checkingPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {

//            Toast.makeText(this, "Permission BAD", Toast.LENGTH_SHORT).show()                     // REMOVE LATER
            ActivityCompat.requestPermissions(this, Constants.PERMISSION_LIST, Constants.PERMISSION_REQUEST_CODE)
        } else {
//            Toast.makeText(this, "Permission OK", Toast.LENGTH_SHORT).show()                      // REMOVE LATER
            setActionListener()
        }
    }

    // ждем нажития кнопки фото + вешаем анимацию на кастомную кнопку для фото
    private fun setActionListener() {
        binding.btnCreatePhoto.setOnClickListener {
            it.startAnimation(AnimationUtils.loadAnimation(this, R.anim.animation_button_press))
            Toast.makeText(this, "КЛАЦ КЛАЦ", Toast.LENGTH_SHORT).show()
            createPhoto()
        }
    }

    // запуск приложения работы с камерой                                                           ( !!! нужен Context )
    private fun createPhoto() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, viewModel.directoryForPhoto())
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivityForResult(intent, Constants.ACTIVITY_REQUEST_CODE)
    }

    // ответ после завершения приложения камеры                                                     (не знаю как получить эти данные во ViewModel)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            binding.ivMainPhoto.setImageURI(viewModel.directoryUri)

            viewModel.addNew(viewModel.directoryUri, adapterRW)
        }
    }

    // сохранение пути к папке с фото в Preferences                                                 ( !!! нет доступа к getPreferences что бы перенести во ViewModel / Model)
    private fun savePreferences(directoryPath: File?) {
        getPreferences(MODE_PRIVATE).edit().putString(Constants.PREFERENCES_KEY, directoryPath.toString()).apply()
        Log.d("TAG", "loadPreferences - COMPLETE")
    }

    // получение пути к папке с фото из Preferences                                                 ( !!! нет доступа к getPreferences что бы перенести во ViewModel / Model)
    private fun loadPreferences() {
        val loadData = getPreferences(MODE_PRIVATE).getString(Constants.PREFERENCES_KEY, null)
        loadData?.let {
            viewModel.loadPhotoDirectoryPath(it)
        }
        Log.d("TAG", "loadPreferences - COMPLETE")
    }

    // получение списка сохраненных файлов в папке с моими фото                                     ( !!! нужен Context )
    private fun getSavedPhoto() {
            viewModel.getSavedPhoto()
    }

    // проверка наличия необходимых разрешений при возобновлении основного экрана
    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkingPermission()
        }
    }

    // показываем диалог удаления и обрабатываем нажатия (удаление и обновление списка)             (нужен доступ к элементам View)
    private fun createPhotoDeleteDialog(position: Int) {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_window)

        // реакция на кнопку "YES"
        dialog.findViewById<Button>(R.id.btnDeleteFileYes).setOnClickListener {

            val fileForDeleteName = viewModel.getPhotoList()[position].toString()
                .substring(viewModel.getPhotoList()[position].toString().lastIndexOf("/") + 1)

            val fileForDelete = File(viewModel.photoDirectoryPath.toString() + "/$fileForDeleteName")

            if (fileForDelete.exists()) {
                val currentPositionImage = viewModel.getPhotoList()[position].toString()            // "запоминаем" текущую картинку
                viewModel.deletePhoto(position, adapterRW)                                          // удаляем файл и обновляем список фото

                fileForDelete.delete()                                                               // удаляем сам файл

                // если удаленный файл сейчас отображается как главное фото,
                // то ставим первое в списке в качестве главного фото
                if (currentPositionImage.equals(viewModel.lastPhoto.value.toString())) {
                    viewModel.lastPhoto.value = viewModel.getPhotoList().last()
                }

                Toast.makeText(this, getString(R.string.toast_info_file_is_deleted), Toast.LENGTH_SHORT).show()     // нет доступа к элементам View из ViewModel
            } else {
                Toast.makeText(this, getString(R.string.toast_info_file_not_deleted), Toast.LENGTH_SHORT).show()      // нет доступа к элементам View из ViewModel
            }
            dialog.dismiss()
        }
        // реакция на кнопку "NO"
        dialog.findViewById<Button>(R.id.btnDeleteFileNo).setOnClickListener {
            viewModel.updateStatus(position, adapterRW)
            dialog.dismiss()
        }

        dialog.setCancelable(false)
        dialog.show()
    }

}